import React, { useState, useEffect } from 'react';
import { Box, Button, Input, Stack, Textarea, FormControl, FormLabel, FormErrorMessage } from '@chakra-ui/react';
import { toast } from 'react-toastify';

const TaskForm = ({ fetchTasks, selectedTask, setSelectedTask }) => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [dueDate, setDueDate] = useState('');
  const [errors, setErrors] = useState({});

  useEffect(() => {
    if (selectedTask) {
      setTitle(selectedTask.title);
      setDescription(selectedTask.description);
      setDueDate(selectedTask.dueDate);
    } else {
      resetForm();
    }
  }, [selectedTask]);

  const resetForm = () => {
    setTitle('');
    setDescription('');
    setDueDate('');
    setErrors({});
  };

  const validate = () => {
    const errors = {};
    if (!title) errors.title = 'Title is required';
    if (!description) errors.description = 'Description is required';
    if (!dueDate) errors.dueDate = 'Due date is required';
    setErrors(errors);
    return Object.keys(errors).length === 0;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!validate()) return;

    const task = { title, description, dueDate };

    try {
      let response;
      if (selectedTask) {
        response = await fetch(`http://localhost:5000/tasks/${selectedTask._id}`, {
          method: 'PUT',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(task),
        });
      } else {
        response = await fetch('http://localhost:5000/tasks', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(task),
        });
      }

      if (response.ok) {
        toast.success(selectedTask ? 'Task updated successfully' : 'Task created successfully');
        fetchTasks();
        resetForm();
        setSelectedTask(null);
      } else {
        const errorData = await response.json();
        toast.error('Failed to save task');
      }
    } catch (error) {
      toast.error('Failed to save task');
    }
  };

  return (
    <Box as="form" onSubmit={handleSubmit} mb={5}>
      <Stack spacing={3}>
        <FormControl isInvalid={errors.title}>
          <FormLabel color={'white'}>Title</FormLabel>
          <Input
            color="white"
            placeholder="Title"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
          <FormErrorMessage>{errors.title}</FormErrorMessage>
        </FormControl>
        <FormControl isInvalid={errors.description}>
          <FormLabel color={'white'}>Description</FormLabel>
          <Textarea
            color="white"
            placeholder="Description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
          <FormErrorMessage>{errors.description}</FormErrorMessage>
        </FormControl>
        <FormControl isInvalid={errors.dueDate}>
          <FormLabel color={'white'}>Due Date</FormLabel>
          <Input
            color="white"
            type="date"
            value={dueDate}
            onChange={(e) => setDueDate(e.target.value)}
          />
          <FormErrorMessage>{errors.dueDate}</FormErrorMessage>
        </FormControl>
        <Button type="submit" colorScheme="teal">
          {selectedTask ? 'Update Task' : 'Add Task'}
        </Button>
      </Stack>
    </Box>
  );
};

export default TaskForm;
