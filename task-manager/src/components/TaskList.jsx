import React from 'react';
import { Box, Button, HStack, Stack, Text, VStack, useToast } from '@chakra-ui/react';

const TaskList = ({ tasks, fetchTasks, setSelectedTask }) => {
  const toast = useToast();

  const deleteTask = async (id) => {
    try {
      const response = await fetch(`http://localhost:5000/tasks/${id}`, {
        method: 'DELETE',
      });

      if (response.ok) {
        toast({
          title: 'Task deleted',
          status: 'success',
          duration: 3000,
          isClosable: true,
        });
        fetchTasks();
      } else {
        const errorData = await response.json();
        console.error('Failed to delete task:', errorData);
        toast({
          title: 'Failed to delete task',
          status: 'error',
          duration: 3000,
          isClosable: true,
        });
      }
    } catch (error) {
      console.error('Failed to delete task:', error);
      toast({
        title: 'Failed to delete task',
        status: 'error',
        duration: 3000,
        isClosable: true,
      });
    }
  };

  return (
    <VStack spacing={4}>
      {tasks.map((task) => (
        <Box
          key={task._id}
          p={4}
          borderWidth="1px"
          borderRadius="md"
          w="100%"
        >
          <Stack spacing={2}>
            <HStack justify="space-between">
              <Text fontSize="lg" fontWeight="bold" color={'white'}>{task.title}</Text>
              <HStack>
                <Button size="sm" onClick={() => setSelectedTask(task)}>Edit</Button>
                <Button size="sm" colorScheme="red" onClick={() => deleteTask(task._id)}>Delete</Button>
              </HStack>
            </HStack>
            <Text color={'white'}>{task.description}</Text>
            <Text fontSize="sm" color="white">Due Date: {task.dueDate}</Text>
          </Stack>
        </Box>
      ))}
    </VStack>
  );
};

export default TaskList;
