import React, { useState, useEffect } from 'react';
import { Container, Heading, VStack, Spinner } from '@chakra-ui/react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import TaskForm from './components/TaskForm';
import TaskList from './components/TaskList';

const App = () => {
  const [tasks, setTasks] = useState([]);
  const [selectedTask, setSelectedTask] = useState(null);
  const [loading, setLoading] = useState(false);

  const fetchTasks = async () => {
    setLoading(true);
    try {
      const response = await fetch('http://localhost:5000/tasks');
      if (!response.ok) {
        throw new Error('Failed to fetch tasks');
      }
      const data = await response.json();
      setTasks(data);
      setLoading(false);
    } catch (error) {
      toast.error(error.message);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchTasks();
  }, []);

  return (
    <div style={{ backgroundColor: '#C3FF93', minHeight: '100vh'}}>
      <Container maxW="container.md" py={{ base: 4, md: 10 }} bgColor="black" px={{ base: 4, md: 10 }}>
        <VStack spacing={8} align="stretch">
          <Heading color="white" textAlign="center" size="lg">Task Management</Heading>
          <TaskForm fetchTasks={fetchTasks} selectedTask={selectedTask} setSelectedTask={setSelectedTask} />
          {loading ? (
            <Spinner size="xl" />
          ) : (
            <TaskList tasks={tasks} fetchTasks={fetchTasks} setSelectedTask={setSelectedTask} />
          )}
          <ToastContainer />
        </VStack>
      </Container>
    </div>
  );
};

export default App;
