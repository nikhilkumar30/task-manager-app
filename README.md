# Task Manager Application

A simple task management application built with React.js frontend and Node.js backend.

## Features

- User can add new tasks with title, description, and due date.
- User can view detailed information of each task.
- User can edit existing tasks.
- User can delete tasks.
- Responsive design for desktop and mobile devices.

## Technologies Used

- React.js
- Node.js
- Express.js
- MongoDB
- Chakra UI

## API Documentation

- **GET /tasks**: Retrieves all tasks.
- **POST /tasks**: Creates a new task.
- **GET /tasks/:id**: Retrieves a task by ID.
- **PUT /tasks/:id**: Updates a task by ID.
- **DELETE /tasks/:id**: Deletes a task by ID.

## Deployment

- Frontend: Deployed on Vercel.
